# Lightweight RESTful Microservices with JAX-RS 2.2

With Microservices as its target, the programmer sees a gap between the typical old-school approach (deploying to an application server) and modern approaches.JAX-RS 2.2 allows to natively startup a web server, even in-process in an IDE, so it allows to simply start it as a Java main() method and debug its startup and requests processing just as easy as Hello World! No more slow build and deploy cycles! This talk demonstrates how to do that, and what more to expect from JAX-RS 2.2 and the further roadmap. JAX-RS is the right choice for professional Microservices, and this talk shows why.

The speaker, a former JSR 339 and JSR 370 expert group member, is one of the most active committers to Jakarta's REST API, authored most of the features demonstrated in this presentation, and knows JAX-RS since more than a decade. So this is talk is the definitive source to learn about the latest features.

## Objective of the presentation:
Attendees will learn how agile and simple writing Microservices becomes once JAX-RS is used natively instead of deploying to a fat application server!

## Attendee pre-requisites:
N/A
