import java.net.URI;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.JAXRS;
import javax.ws.rs.JAXRS.Configuration;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.UriBuilder;

public class HelloWorldBootstrap {
	public static void main(final String[] args) throws InterruptedException, ExecutionException {
		final int port = Integer.parseInt(System.getenv().getOrDefault("IP_PORT", "0"));

		final Application application = new HelloWorldApplication();

		final Configuration configuration = Configuration.builder().protocol("http").host("localhost").port(port).build();

		JAXRS.start(application, configuration).thenAccept(instance -> {

			Runtime.getRuntime().addShutdownHook(new Thread(() -> instance.stop()
					.thenAccept(stopResult -> System.err.printf("Stop result: %s [Native stop result: %s]%n", stopResult, stopResult.unwrap(Object.class)))));

			final Configuration actualConfigurarion = instance.configuration();
			final URI uri = UriBuilder.newInstance().scheme(actualConfigurarion.protocol().toLowerCase()).host(actualConfigurarion.host())
					.port(actualConfigurarion.port()).path(actualConfigurarion.rootPath()).build();

			/*
			 * AppCDS: Stop immediately once service is up and running
			 */
			if (args.length > 0 && "--stop".equals(args[0])) {
				instance.stop();
				System.out.println("Stopping immediately...");
				System.exit(0);
				return;
			}

			System.out.printf("Listening to %s - Send SIGKILL to shutdown.%n", uri);
		});

		Thread.currentThread().join();
	}
}
